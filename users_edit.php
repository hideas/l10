<?php
session_start();
header("Content-Type:text/html;charset=utf8");

require "config.php";
require "functions.php";

if (isset($_POST['reg'])) {
    $msg = edit_users($_POST);

    if ($msg === TRUE) {
        $_SESSION['msg'] = "Пользователь отредактирован. " . " " .
            "Нажмите <a href='users_view.php'>назад</a>" . ", чтобы посмотреть";
    } else {
        $_SESSION['msg'] = $msg;
    }
    header("Location:" . $_SERVER['PHP_SELF']);
    exit();
}

?>
<? include "inc/header.php";?>
    <div id="content">
    <div id="main">
        <h1>Редактирование пользователя</h1>
        <?= $_SESSION['msg']; ?>
        <? unset($_SESSION['msg']); ?>

        <form method='POST'>
            <em>Введите старую почту для поиска в базе</em><br>
            <input type='text' name='reg_old_email'>
            <br>
            Логин<br>
            <input type='text' name='reg_login' value="<?=$_SESSION['reg']['login'];?>">
            <br>
            Пароль<br>
            <input type='password' name='reg_password'>
            <br>
            Новая почта<br>
            <input type='text' name='reg_email' value="<?=$_SESSION['reg']['email'];?>">
            <br>
            Имя<br>
            <input type='text' name='reg_name' value="<?=$_SESSION['reg']['name'];?>">
            <br>
            <input style="float:left" type='submit' name='reg' value='Редактировать'>
        </form>
        <br><p>
            <a href="users_view.php">Назад</a>
        </p>
    </div>
<? include "inc/sidebar.php";?>

<? include "inc/footer.php";?>

<? unset($_SESSION['reg']); ?>