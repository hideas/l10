<?php
session_start();
header("Content-Type:text/html;charset=utf8");

require "config.php";
require "functions.php";

if (isset($_POST['email'])) {
    $msg = delete_users($_POST['email']);

    if ($msg === TRUE){
        $_SESSION['msg'] = "Пользователь удален из базы. " . " " .
            "Нажмите <a href='users_view.php'>назад</a>" . ", чтобы посмотреть";
    }
    else{
        $_SESSION['msg'] = $msg;
    }
    header("Location:".$_SERVER['PHP_SELF']);
    exit();
}

?>
<? include "inc/header.php";?>
<div id="content">
    <div id="main">
        <h1>Удаление пользователя</h1>
        <?= $_SESSION['msg']; ?>
        <? unset($_SESSION['msg']); ?>

        <form method='POST'>
            Введите почту пользователя<br>
            <input type='text' name='email' value="<?=$_SESSION['email'];?>">
            <br>
            <input style="float:left" type='submit' name='reg' value='Удалить'>
        </form>
        <br><p>
            <a href="users_view.php">Назад</a>
        </p>
    </div>
    <? include "inc/sidebar.php";?>

    <? include "inc/footer.php";?>


