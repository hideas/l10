<?php
session_start();
header("Content-Type:text/html;charset=utf8");

require ("config.php");
require ("functions.php");

/*
Есть ячейка из пост-запроса
  мы должны получить новый пароль get_password
  Если отработала генерируем сообщение для пользователя (в сессию)
  переброску на страницу login

Если нет ячейки то , редирект на саму себя

*/
if(isset($_POST['email'])) {
	$msg = get_password($_POST['email']);
	
	if($msg === TRUE) {
		$_SESSION['msg'] = "Новый пароль выслан вам на почту";
		header("Location:login.php");
	}
	else {
		$_SESSION['msg'] = $msg;
		header("Location:".$_SERVER['PHP_SELF']);
	}
	exit();
}
/*
else {
    header("Location:".$_SERVER['PHP_SELF']);
    exit();
}*/


?>
<? include "inc/header.php";?>
	<div id="content">	
		<div id="main">
                <?=$_SESSION['msg'];?>
				<? unset($_SESSION['msg'])?>
			<h1>Введите свой почтовый адрес</h1>
            <form method='POST'>
                <label>Email<br>
                <input type="text" name='email'>
                <br>
                </label>
                <input type="submit" value='Отправить' style="float:left">
            </form>	
		</div>
<? include "inc/sidebar.php";?>		
	
<? include "inc/footer.php";?>


