<?php


function get_statti() {
	global $db;
	$sql = "SELECT * FROM statti";
	$result = mysqli_query($db, $sql);
		
	if(!$result) {
		exit(mysqli_error());
	}
	
	for($i = 0; $i<mysqli_num_rows($result);$i++) {
		$row[] = mysqli_fetch_array($result);
	}
	return $row;
}

function registration($post){

	

	$login = clean_data($post['reg_login']);
	$password = trim($post['reg_password']);
	$conf_pass = trim($post['reg_password_confirm']);
	$email = clean_data($post['reg_email']);
	$name = clean_data($post['reg_name']);

	$msg = '';
	if (empty($login)){
		$msg .="Введите логин! <br />";
	}
	if (empty($password)){
		$msg .="Введите пароль! <br />";
	}
	if (empty($email)){
		$msg .="Введите email! <br />";
	}
	if (empty($name)){
		$msg .="Введите имя! <br />";
	}
	
	if ($msg){
		$_SESSION['reg']['login'] = $login;
		$_SESSION['reg']['email'] = $email;
		$_SESSION['reg']['name'] = $name;
		return $msg;
	}


	if($conf_pass == $password) {
		global $db;

		$sql = "SELECT user_id
					FROM users
					WHERE login='%s'";
		$sql = sprintf($sql,mysqli_real_escape_string($db, $login));
			
		$result = mysqli_query($db, $sql);
			
		if(mysqli_num_rows($result) > 0) {
				$_SESSION['reg']['email'] = $email;
				$_SESSION['reg']['name'] = $name;
				
				return "Пользователь с таким логином уже существует";
		}
		$password = md5($password);
			$hash = md5(microtime());//текущая метка времени в милсекундах
			
			$query = "INSERT INTO users (
						name,
						email,
						password,
						login,
						hash
						) 
					VALUES (
						'%s',
						'%s',
						'%s',
						'%s',
						'$hash'
					)";
			$query = sprintf($query,
								mysqli_real_escape_string($db, $name),
								mysqli_real_escape_string($db, $email),
								$password,
								mysqli_real_escape_string($db, $login)
							);
			$result2 = mysqli_query($db, $query);

			if(!$result2) {
				$_SESSION['reg']['login'] = $login;
				$_SESSION['reg']['email'] = $email;
				$_SESSION['reg']['name'] = $name;
				return "Ошибка при добавлении пользователя в базу данных".mysqli_error($db);
			}
			else{
				$headers = '';
				$headers .= "From: Admin <admin@ukr.net> \r\n";
				$headers .= "Content-Type: text/plain; charset=utf8";
				
				$tema = "Регистрация";
				
				$mail_body = "Спасибо за регистрацию на сайте. "
							."Ваша ссылка для подтверждения  учетной записи: "
							."http://l10/confirm.php?hash=".$hash;
				
				mail($email,$tema,$mail_body,$headers);
				
				return TRUE;
			}				
	}
	else {
		$_SESSION['reg']['login'] = $login;
		$_SESSION['reg']['email'] = $email;
		$_SESSION['reg']['name'] = $name;
		return "Вы неправильно подтвердили пароль";
	}
}


function clean_data($str){
	return strip_tags(trim($str));
}


function confirm(){
	global $db;
	$new_hash  = clean_data($_GET['hash']);

	$query = "
			UPDATE users 
			SET confirm = '1'
			WHERE hash = '%s'
	";
	$query = sprintf($query,mysqli_real_escape_string($db, $new_hash));	
	
	$resutl = mysqli_query($db, $query);
	
	if(mysqli_affected_rows($db) == 1) {
		return TRUE;
	}
	else {
		return "Неправильный код подтверждения регистрации";
	}

}

function check_user(){
	global $db;
	if (isset($_SESSION['sess'])){
		$sess = $_SESSION['sess'];

		$sql = "SELECT user_id
				FROM users
				WHERE sess='$sess'";
		$result = mysqli_query($db, $sql);

		if(!$result || mysqli_num_rows($result) < 1){
			return FALSE;
		}
		if (isset($_COOKIE['login']) 
			&& isset($_COOKIE['password'])){
				$login = $_COOKIE['login'];
				$password = $_COOKIE['password'];

				$sql = "
					SELECT user_id
					FROM users
					WHERE login='$login'
					AND password = '$password'
					AND confirm = '1';
				";
				$result2 = mysqli_query($db, $sql);
				if(!$result2 || 
					mysqli_num_rows($result2) < 1){
					return FALSE;
				}
				$sess = md5(microtime());

				$sql_update = "
					UPDATE users SET sess='$sess'
					WHERE login='%s'";
					$sql_update = 
				sprintf(
					$sql_update,
					mysqli_real_escape_string($db, $login));
				
				if(!mysqli_query($db, $sql_update)) {
					return "Ошибка авторизации пользователя";
				}				
			}
			$_SESSION['sess'] = $sess;

			return TRUE;		
		}
		return FALSE;
}

function login($post){
	if (empty($post['login']) || empty($post['password'])){
		return "Заполните поля!!!";
	}

	$login = clean_data($post['login']);
	$password = md5(trim($post['password']));

	global $db;
	$sql = "SELECT user_id,confirm
			FROM users
			WHERE login = '%s'
			AND password = '%s'";
	$sql = sprintf($sql,mysqli_real_escape_string($db, $login),$password);
	
	$result = mysqli_query($db,$sql);

	if(!$result || mysqli_num_rows($result) < 1) {
		return "Неправильный логин или пароль";
	}

	$data = mysqli_fetch_all($result, MYSQLI_ASSOC);
	
	if ($data[0]['confirm'] == 0){
		return "Пользователь с таким логином ещё не продтверждён";
	}
	
	$sess = md5(microtime());

	$sql_update = "UPDATE users SET sess='$sess' WHERE login='%s'";
	$sql_update = sprintf($sql_update,mysqli_real_escape_string($db, $login));
	
	if(!mysqli_query($db, $sql_update)) {
		return "Ошибка авторизации пользователя";
	}
	
	$_SESSION['sess'] = $sess;

	if ($post['member'] == 1){
		$time = time() + 10*24*60*60;

		setcookie('login',$login,$time);
		setcookie('password',$password,$time);
	}

	return TRUE;
}

function logout(){
	unset($_SESSION['sess']);
	setcookie('login', '', time() - 3600);
	setcookie('password', '', time() - 3600);
	return TRUE;
}

	
function get_password($email) {
	/*
	Если в БД есть email
	  то генерир
	  {
		1. Генерируем 
		2. обновл в БД
			если не получ. обновить пароль в БД
			 отправить сообщение 
			     что не получ генерировать пароль
		3. Создаем сообщение с паролем
		4. Отправл. сообщение
	  }
	  если нет {
		  отправляем ссобщение пользователю
		  что нет такого email
	  }


	*/

	global $db;
	$email = clean_data($email);

	$sql = "SELECT user_id
			FROM users
			WHERE email = '%s'";
	$sql = sprintf($sql,mysqli_real_escape_string($db, $email));	
	
	$result = mysqli_query($db, $sql);
	
	if(!$result) {
		return "Невозможно сгенерировать новый пароль";
	}

	if(mysqli_num_rows($result) >= 1) {
		$str = "1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";
		
		$pass = '';
		for($i = 0; $i < rand(3, 8); $i++) {
			$x = mt_rand(0,(strlen($str)-1));
			
			if($i != 0) {
				if($pass[strlen($str)-1] == $str[$x]) {
					$i--;
					continue;
				}
			}
			$pass .= $str[$x];
		}
		$md5pass = md5($pass);

		$data = mysqli_fetch_all($result, MYSQLI_ASSOC);
		
		$query = "UPDATE users
					SET password='$md5pass'
					WHERE user_id = '".$data[0]['user_id']."'";
		$result2 = mysqli_query($db, $query);
		
		if(!$result2) {
			return "Невозможно сгенерировать новый пароль";
		}

		$headers = '';
		$headers .= "From: Admin <admin@mail.ru> \r\n";
		$headers .= "Content-Type: text/plain; charset=utf8";
		
		$subject = 'new password';
		$mail_body = "Ваш новый пароль: ".$pass;

		mail($email,$subject,$mail_body,$headers);
		
		return TRUE;

	}
	else {
		return "Пользователя с таким почтовым ящиком нет";
	}	
}

function get_users() {
    global $db;
    $sql = "SELECT * FROM users";
    $result = mysqli_query($db, $sql);

    if (!$result) {
        exit(mysqli_error());
    }

    for ($i = 0; $i < mysqli_num_rows($result); $i++) {
        $row[] = mysqli_fetch_array($result);
    }
    return $row;
}

function add_statti($post){
    $title = $post['title'];
    $author = $post['author'];
    $date = date('Y-m-d');
    $img_src = $post['img_src'];
    $discription = $post['discription'];

    $msg = '';
    if (empty($title)){
        $msg .="Введите заголовок! <br />";
    }
    if (empty($author)){
        $msg .="Укажите автора! <br />";
    }
    if (empty($img_src)){
        $msg .="Укажите ссылку на картинку! <br />";
    }
    if (empty($discription)){
        $msg .="Введите описание! <br />";
    } else{
        global $db;

        $sql = "SELECT id
          FROM statti
          WHERE title='%s'";
        $sql = sprintf($sql,mysqli_real_escape_string($db, $title));
        $result = mysqli_query($db, $sql);

        if(mysqli_num_rows($result) > 0) {
            return "Такая запись уже существует";
        }

        $query = "INSERT INTO statti (
            title,
            author,
            date,
            img_src,
            discription
            ) 
          VALUES (
            '%s',
            '%s',
            '%s',
            '%s',
            '%s'
          )";
        $query = sprintf($query,
            $title,$author,$date,$img_src,$discription
        );
        $result2 = mysqli_query($db, $query);

        if(!$result2) {
            return "Ошибка при добавлении статьи в базу данных";
        }
    }
    return TRUE;
}

function delete_users($email) {
    $msg = '';
    if (empty($email)){
        $msg .="Введите почту! <br />";
        return $msg;
    } else{
        global $db;
        $email = clean_data($email);

        $sql = "DELETE FROM users 
                WHERE email='$email'";
        $sql = sprintf($sql,mysqli_real_escape_string($db, $email));

        $result = mysqli_query($db, $sql);

        if(!$result) {
            return "Невозможно удалить пользователя";
        }
    }
    return TRUE;
}

function delete_statti($title) {
    $msg = '';
    if (empty($title)){
        $msg .="Введите заголовок! <br />";
        return $msg;
    } else{
        global $db;
        $title = clean_data($title);

        $sql = "DELETE FROM statti 
                WHERE title='%s'";
        $sql = sprintf($sql,mysqli_real_escape_string($db, $title));

        $result = mysqli_query($db, $sql);

        if(!$result) {
            return "Невозможно удалить статью";
        }
    }
    return TRUE;
}

function edit_users($post){
    $old_email = clean_data($post['reg_old_email']);
    $login = clean_data($post['reg_login']);
    $password = trim($post['reg_password']);
    $email = clean_data($post['reg_email']);
    $name = clean_data($post['reg_name']);

    $msg = '';
    if (empty($old_email)){
        $msg .="Введите почту для поиска! <br />";
        return $msg;
    }
    global $db;

    $sql = "SELECT email
					FROM users
					WHERE email='%s'";
    $sql = sprintf($sql,mysqli_real_escape_string($db, $old_email));

    $result = mysqli_query($db, $sql);

    if (!$result){
        return "Такого пользователя нет";
    }else{
        $hash = md5(microtime());

        if (!empty($login)){
            $query = "UPDATE users
					SET login = '$login',
					hash = '$hash'
					WHERE email='%s'";
            $query = sprintf($query,mysqli_real_escape_string($db, $old_email));

            $result2 = mysqli_query($db, $query);
        }
        if (!empty($password)){
            $query = "UPDATE users
					SET password = '$password',
					hash = '$hash'
					WHERE email='%s'";
            $query = sprintf($query,mysqli_real_escape_string($db, $old_email));

            $result2 = mysqli_query($db, $query);
        }
        if (!empty($email)){
            $query = "UPDATE users
					SET email = '$email',
					hash = '$hash'
					WHERE email='%s'";
            $query = sprintf($query,mysqli_real_escape_string($db, $old_email));

            $result2 = mysqli_query($db, $query);
        }
        if (!empty($name)){
            $query = "UPDATE users
					SET name = '$name',
					hash = '$hash'
					WHERE email='%s'";
            $query = sprintf($query,mysqli_real_escape_string($db, $old_email));

            $result2 = mysqli_query($db, $query);
        }

        if(!$result2) {
            return "Невозможно или нечего редактировать";
        }
    }
    return TRUE;
}

function edit_statti($post){
    $old_title = clean_data($post['old_title']);
    $title = $post['title'];
    $author = $post['author'];
    $date = $post['date'];
    $img_src = $post['img_src'];
    $discription = $post['discription'];

    $msg = '';
    if (empty($old_title)){
        $msg .="Введите заголовок для поиска! <br />";
        return $msg;
    }
    global $db;

    $sql = "SELECT title
					FROM statti
					WHERE title='%s'";
    $sql = sprintf($sql,mysqli_real_escape_string($db, $old_title));

    $result = mysqli_query($db, $sql);

    if (!$result){
        return "Такой статьи нет";
    }else{
        if (!empty($title)){
            $query = "UPDATE statti
					SET title = '$title'
					WHERE title='%s'";
            $query = sprintf($query,mysqli_real_escape_string($db, $old_title));

            $result2 = mysqli_query($db, $query);
        }
        if (!empty($author)){
            $query = "UPDATE statti
					SET author = '$author'
					WHERE title='%s'";
            $query = sprintf($query,mysqli_real_escape_string($db, $old_title));

            $result2 = mysqli_query($db, $query);
        }
        if (!empty($date)){
            $query = "UPDATE statti
					SET date = '$date'
					WHERE title='%s'";
            $query = sprintf($query,mysqli_real_escape_string($db, $old_title));

            $result2 = mysqli_query($db, $query);
        }
        if (!empty($img_src)){
            $query = "UPDATE statti
					SET img_src = '$img_src'
					WHERE title='%s'";
            $query = sprintf($query,mysqli_real_escape_string($db, $old_title));

            $result2 = mysqli_query($db, $query);
        }
        if (!empty($discription)){
            $query = "UPDATE statti
					SET discription = '$discription'
					WHERE title='%s'";
            $query = sprintf($query,mysqli_real_escape_string($db, $old_title));

            $result2 = mysqli_query($db, $query);
        }

        if(!$result2) {
            return "Невозможно или нечего редактировать";
        }
    }
    return TRUE;
}




