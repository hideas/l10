<?php
session_start();
header("Content-Type:text/html;charset=utf8");

require "config.php";
require "functions.php";

if (isset($_POST['title'])) {
    $msg = delete_statti($_POST['title']);

    if ($msg === TRUE){
        $_SESSION['msg'] = "Статья удалена из базы. " . " " .
            "Нажмите <a href='statti_view.php'>назад</a>" . ", чтобы посмотреть";
    }
    else{
        $_SESSION['msg'] = $msg;
    }
    header("Location:".$_SERVER['PHP_SELF']);
    exit();
}

?>
<? include "inc/header.php";?>
<div id="content">
    <div id="main">
        <h1>Удаление статьи</h1>
        <?= $_SESSION['msg']; ?>
        <? unset($_SESSION['msg']); ?>

        <form method='POST'>
            Введите заголовок статьи<br>
            <input type='text' name='title' value="<?=$_SESSION['title'];?>">
            <br>
            <input style="float:left" type='submit' name='reg' value='Удалить'>
        </form>
        <br><p>
            <a href="statti_view.php">Назад</a>
        </p>
    </div>
    <? include "inc/sidebar.php";?>

    <? include "inc/footer.php";?>


