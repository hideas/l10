<?php
session_start();
header("Content-Type:text/html;charset=utf8");

require "config.php";
require "functions.php";

if (isset($_POST['reg'])) {
    $msg = edit_statti($_POST);

    if ($msg === TRUE) {
        $_SESSION['msg'] = "Статья отредактирована. " . " " .
            "Нажмите <a href='statti_view.php'>назад</a>" . ", чтобы посмотреть";
    } else {
        $_SESSION['msg'] = $msg;
    }
    header("Location:" . $_SERVER['PHP_SELF']);
    exit();
}

?>
<? include "inc/header.php";?>
    <div id="content">
    <div id="main">
        <h1>Редактирование статьи</h1>
        <?= $_SESSION['msg']; ?>
        <? unset($_SESSION['msg']); ?>

        <form method='POST'>
            <em>Введите старый заголовок для поиска в базе</em><br>
            <input type='text' name='old_title' value="">
            <br>
            Новый заголовок<br>
            <input type='text' name='title' value="">
            <br>
            Автор<br>
            <input type='text' name='author' value="">
            <br>
            Дата<br>
            <input type='text' name='date' value="">
            <br>
            Изображение<br>
            <input type='text' name='img_src' value="">
            <br>
            Содержание<br>
            <input type='text' name='discription' value="">
            <br>
            <input style="float:left" type='submit' name='reg' value='Редактировать'>
        </form>
        <br><p>
            <a href="statti_view.php">Назад</a>
        </p>
    </div>
<? include "inc/sidebar.php";?>

<? include "inc/footer.php";?>

<? unset($_SESSION['reg']); ?>